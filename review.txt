Problem 1 (General knowledge)
-----------------------------

PM/0 machine

sp stack pointer
bp base pointer
ir instruction register
pc program counter

activation record

what is stored in an activation record

parameters       
variables        .
return address   .
dynamic link     .
static link      .
functional value

What is stored in the symbol table?

information about identifiers, constants, and procedures

for each of the three types belown we have the kind field

identifier:
  name
  lexicographical level
  address offset

constant:
  name
  value

procedure:
  name
  lexicographical level
  address


Problem 2 (Code generation)
---------------------------

What restrictions are there on the caller and callee
                                                          level difference     L in CAL L M
- a) caller calls its child (but not grandchild etc)           -1                0
- b) caller calls itself or its sibling                         0                1
- c) caller calls parent                                        1                2
- d) caller calls call grandparent                              2                3
- etc.                     

a fragment of PL/0 code

procedure caller;
  ...                emit 
  call callee;        ~~>     CAL L M
  ...                code

L in CAL L M is given by L = 1 + level difference (see slides on static link)

write the assembler instruction (instead of the number triples op, level, modifier)

LIT   M
LOD L M
STO L M

JMP   M
JPC   M

CAL L M

IN
OUT

ADD
SUB
MUL 
DIV

LSS
LEQ
GTR
GEQ

if (x < y) then .....

LOD L(x) M(x)
LOD L(y) M(y)
LSS
JPC      M



  




Problem 3 (Synatx diagram)
--------------------------




